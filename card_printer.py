from card import Card
from translate_suit import translateSuit
from typing import List


def playerCardsPrinter(cards: List[Card]):
    for card in cards:
        if card.rank == 14:
            card.rank = 'A'
        elif card.rank == 13:
            card.rank = 'K'
        elif card.rank == 12:
            card.rank = 'Q'
        elif card.rank == 11:
            card.rank = 'J'

    if cards[0].rank == 10 and cards[1].rank == 10:
        return '\n----------\t----------\n| {}      |\t| {}      |\n|         |\t|         |\n|    {}    |\t|    {}    |\n|         |\t|         |\n|      {} |\t|      {} |\n----------\t----------'.format(cards[0].rank,cards[1].rank, translateSuit(cards[0].suit), translateSuit(cards[1].suit), cards[0].rank,cards[1].rank)
    elif cards[0].rank == 10:
        return '\n----------\t----------\n| {}      |\t| {}       |\n|         |\t|         |\n|    {}    |\t|    {}    |\n|         |\t|         |\n|      {} |\t|       {} |\n----------\t----------'.format(cards[0].rank,cards[1].rank, translateSuit(cards[0].suit), translateSuit(cards[1].suit), cards[0].rank,cards[1].rank)
    elif cards[1].rank == 10:
        return '\n----------\t----------\n| {}       |\t| {}      |\n|         |\t|         |\n|    {}    |\t|    {}    |\n|         |\t|         |\n|       {} |\t|      {} |\n----------\t----------'.format(cards[0].rank,cards[1].rank, translateSuit(cards[0].suit), translateSuit(cards[1].suit), cards[0].rank,cards[1].rank)
    else:
        return '\n----------\t----------\n| {}       |\t| {}       |\n|         |\t|         |\n|    {}    |\t|    {}    |\n|         |\t|         |\n|       {} |\t|       {} |\n----------\t----------'.format(cards[0].rank,cards[1].rank, translateSuit(cards[0].suit), translateSuit(cards[1].suit), cards[0].rank,cards[1].rank)
    
    
    
    
def printTableCards(cards: List[Card]):
    finalString = ''
    for card in cards:
        card.suit = translateSuit(card.suit)
        if card.rank == 14:
            card.rank = 'A'
        elif card.rank == 13:
            card.rank = 'K'
        elif card.rank == 12:
            card.rank = 'Q'
        elif card.rank == 11:
            card.rank = 'J'
        finalString += '|{}{}|'.format(card.rank, card.suit)
    return finalString
    