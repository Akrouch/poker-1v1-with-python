from card import Card
import string, math, random

class Deck (object):
  def __init__ (self):
    self.deck = []
    for suit in Card.SUITS:
      for rank in Card.RANKS:
        card = Card(rank, suit)
        self.deck.append(card)

  def shuffle(self):
    random.shuffle(self.deck)

  def __len__(self):
    return len(self.deck)

  def deal(self):
    if len(self) == 0:
      return None
    else:
      return self.deck.pop(0)
  
  def removeCard(self, card):
      for x in range(len(self.deck)):
          if self.deck[x].rank == card.rank and self.deck[x].suit == card.suit:
             return self.deck.pop(x)

      
      