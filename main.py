from card import Card
from deck import Deck
from player import Player
import math
from calculate_equity import calculate_equity
import os
from time import sleep

print("♥")
deck = Deck()
deck.shuffle()
player1 = Player('LUANESSON', deck.deal(), deck.deal())
player2 = Player('KROTE', deck.deal(), deck.deal())
input('Show player {}\'s cards?'.format(player1.name))
print(player1.__str__())
sleep(2)
os.system('clear')
input('Show player {}\'s cards?'.format(player2.name))
print(player2.__str__())
sleep(2)

os.system('clear')
a = input('Show flop?')
calculate_equity([player1, player2], deck)

