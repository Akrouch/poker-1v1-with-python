from card import Card
from card_printer import playerCardsPrinter
class Player(object):
    
    def __init__ (self, name, card1, card2):
        self.name = name
        self.card1 = Card(card1.rank, card1.suit)
        self.card2 = Card(card2.rank, card2.suit)
    
    def __str__(self): 
        return playerCardsPrinter([self.card1, self.card2])