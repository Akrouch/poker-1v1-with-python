def translateSuit(suit):
    new = suit
    if suit == 's':
        new = "♠"
    elif suit == 'c':
        new = "♣"
    elif suit == 'd':
        new = "♦"
    elif suit == 'h':
        new = "♥"
    return new 